1. `clone https://github.com/xu-song/bert-as-language-model`
2. install the requirements
3. `mkdir model`
4. move your bert model in folder.
>  I use [This BERT](https://storage.googleapis.com/bert_models/2018_11_23/multi_cased_L-12_H-768_A-12.zip)
5. `export BERT_BASE_DIR=model/<your_model_dir>`
6. `mkdir results`
7. > in `run_lm_predict.py` at line 0 add
  `import time`
> in `run_lm_predict.py` at line 524 change
-  `output_predict_file = os.path.join(FLAGS.output_dir, "test_results.json") `
> to
- `output_predict_file = os.path.join(FLAGS.output_dir, "test_results"+str(time.clock())+".json")`

8. Generate data:

    1. use parsed data or parse using parser.py
-  [book](https://pdftotext-converter.online/ru/converted/af2bdbc9/ep-senichkina-slovar-evfemizmov-russkogo-yazyka/v1pgnsfaiksmej8b1ged0ljywiaclyuwiuenxijfpdf.pdf)
    
    
    2. run jupyter notebook morph.ipynb for batch generation (evfims/file[n].tsv)

9. Use BERT to get probabilities. 
    run this bash script:
    
```
max_n=5953 ; for n in $(seq 0 $max_n) ; do echo running $n / $max_n ... ; python run_lm_predict.py \
  --input_file=data/evfims/text$n.ru.tsv \
  --vocab_file=$BERT_BASE_DIR/vocab.txt \
  --bert_config_file=$BERT_BASE_DIR/bert_config.json \
  --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt \
  --max_seq_length=128 \
  --output_dir=results/ ; \
done
```


10. Data analysis:
    Probability evaluation takes some time, so you might start analysing the already evaluated probs using json_analytics.ipynb even before all the files are finished.

Done