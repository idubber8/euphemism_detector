import re
import sys
from collections import namedtuple


DASH = ' – '
def merge_definitions(lines):
    name = None
    queue = []
    for line in lines:
        line = line.strip()
        if DASH in line:
            new_name, first_line = line.split(DASH, 1)
            if new_name and new_name.upper() == new_name:
                if queue:
                    yield name, ' '.join(queue)
                queue = [first_line]
                name = new_name
                continue
        queue.append(line)
    if queue:
        yield name, ' '.join(queue)
    

def parse_sections(section_titles, lines):
    section_rows = []
    prev_title = None
    for title in section_titles:
        for line in lines:
            if line.strip().endswith(title):
                if prev_title:
                    yield section_rows
                section_rows = []
                prev_title = title
                break
            else:
                section_rows.append(line)
    yield list(lines)


BREAKING_SUFFIXES = ['-', '+', '', '(']
def merge_broken_lines(lines):
    queue = []
    for line in lines:
        if line and line[-1] in BREAKING_SUFFIXES:
            queue.append(line[:-1])
        else:
            queue.append(line)
            yield ''.join(queue)
            queue = []
        

def extract_columns(raw_lines):
    row_queue = []
    for raw_line in raw_lines:
        raw_line = raw_line.strip()
        if '  ' in raw_line:  # and and
            row_queue.append(tuple(cell.strip() for cell in raw_line.split('  ') if cell.strip()))
        else:
            for line in use_queue(row_queue):
                yield line
            row_queue = []
    for line in use_queue(row_queue):
        yield line


def use_queue(row_queue):
    if row_queue:
        for col in range(max(map(len, row_queue))):
            for row in row_queue:
                if len(row) > col:
                    yield row[col]


def replace_brackets(line):
    return re.sub('\(.*?\)', '', line)


def replace_chars(lines):
    for line in lines:
        yield line.replace('%', '-').replace('+', '-').replace('', '-').replace('\t', ' ').replace('´ ', '')

def extract_evfs(evf_lines, abbr_to_definition):
    evf_lines = merge_broken_lines(extract_columns(replace_chars(evf_lines)))
    for evf, definition in merge_definitions(evf_lines):
        if len(evf) > 2 and evf[-1] == '.' and evf[-2] in '123456789':
            evf = evf[:-3]

        evf = replace_brackets(evf)
        definition = replace_brackets(definition)
        definition = re.sub('СР:.*', '', definition)
        definition = definition.replace('/', '')

        tokens = [token for token in definition.split() if not token.endswith('-л')]
        for i, token in enumerate(tokens):
            if not token in abbr_to_definition:
                break
        abbr_part = ' '.join(tokens[:i])
        rest_part = ' '.join(tokens[i:])
        meaning_part, rest_part = (rest_part.split('.', 1) + [''])[:2]
        
        example_part = ' '.join(tokens[max([i + 1 for i, token in enumerate(tokens) if token in abbr_to_definition], default=0):])

        for single_meaning in meaning_part.split(','):
            yield evf.replace('(', '-'), single_meaning, example_part


def main():
    sources_lines, abbr_lines, evf_lines = parse_sections(
        ['Периодические издания', 'В СЛОВАРЕ СОКРАЩЕНИЙ', 'АДЕКВАТНАЯ РЕАКЦИЯ'],
        sys.stdin
    )

    alias_to_source = {alias.strip(): source.strip() for source, alias in 
                       (line.split(DASH, 1) for line in sources_lines if DASH in line)}
    
    abbr_lines = merge_broken_lines(extract_columns(replace_chars(abbr_lines)))
    abbr_to_definition = {abbr.strip(): definition.strip() for abbr, definition in
                          (line.split(DASH, 1) for line in abbr_lines if DASH in line)}
    for full in ['наименования', 'обозначения', 'в.']:
        abbr_to_definition[full] = full

    for evf, meaning, example in extract_evfs(evf_lines, abbr_to_definition):
        print('\t'.join([evf, meaning, example]))
    

if __name__ == '__main__':
    main()
